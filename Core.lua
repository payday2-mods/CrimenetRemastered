
if not CrimenetRemastered then
	CrimenetRemastered = {}
	CrimenetRemastered._path = ModPath
	CrimenetRemastered._lua_path = ModPath .. "Lua/"
	CrimenetRemastered._defaults_path = ModPath .. "default_config.json"
	CrimenetRemastered._options_path = SavePath .. "CrimenetRemastered.json"
	CrimenetRemastered._data = {}
	CrimenetRemastered._data_old = {}

	CrimenetRemastered._hook_files = {
		["lib/managers/crimenetmanager"]					= {"CrimenetManager.lua"},
		["lib/managers/localizationmanager"]				= {"LocalizationManager.lua"},
		["lib/managers/menumanager"]						= {"MenuManager.lua"},
		["lib/managers/menu/items/contractbrokerheistitem"]	= {"ContractBrokerHeistItem.lua"},
		["lib/tweak_data/guitweakdata"]						= {"GuiTweakData.lua"},
		["lib/tweak_data/narrativetweakdata"]				= {"NarrativeTweakData.lua"}
	}

	CrimenetRemastered.diff_colors = {
		Color(0/6, 6/6, 0),	-- normal
		Color(1/6, 5/6, 0),	-- hard
		Color(2/6, 4/6, 0),	-- very hard
		Color(3/6, 3/6, 0),	-- overkill
		Color(4/6, 2/6, 0),	-- mayhem
		Color(5/6, 1/6, 0),	-- deathwish
		Color(6/6, 0/6, 0)	-- one down
	}

	function CrimenetRemastered:Load()
		local defaults = self:GetDefaults()
		self._data = deep_clone(defaults)
		local stream = io.open(self._options_path, "r")
		if stream then
			local data = json.decode(stream:read("*all"))
			stream:close()
			self._data_old = deep_clone(data)
			for k, v in pairs(data) do
				if defaults[k] ~= nil then
					self._data[k] = v
				end
			end
		end
		self:Save()
	end

	function CrimenetRemastered:Save()
		if self:ConfigChanged() then
			local stream = io.open(self._options_path, "w+")
			if stream then
				stream:write(json.encode(self._data))
				stream:close()
			end
		end
	end

	function CrimenetRemastered:GetDefaults()
		local result
		local stream = io.open(self._defaults_path, "r")
		if stream then
			result = json.decode(stream:read("*all"))
			stream:close()
		end
		return result
	end

	function CrimenetRemastered:ConfigChanged()
		for k, _ in pairs(self._data_old) do
			if (self._data_old[k] ~= self._data[k]) then
				return true
			end
		end
		for k, _ in pairs(self._data) do
			if (self._data_old[k] ~= self._data[k]) then
				return true
			end
		end
		return false
	end

	function CrimenetRemastered:GetOption(id)
		return self._data[id]
	end

	CrimenetRemastered:Load()
	CrimenetRemastered.setup = true
end

if RequiredScript then
	local hook_files = CrimenetRemastered._hook_files[RequiredScript:lower()]
	if hook_files then
		if type(hook_files) == "string" then
			hook_files = {hook_files}
		end
		for _, file in pairs(hook_files) do
			dofile(CrimenetRemastered._lua_path .. file)
		end
	end
end
