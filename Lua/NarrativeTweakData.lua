
--[[
local create_job_name_original = NarrativeTweakData.create_job_name
function NarrativeTweakData:create_job_name(job_id, ...)
	local text_id, color_ranges = create_job_name_original(self, job_id, ...)
	local job_tweak = self:job_data(job_id)
	if job_tweak.dlc and (not tweak_data.dlc[job_tweak.dlc] or not tweak_data.dlc[job_tweak.dlc].free) then
		if CrimenetRemastered:GetOption("brkr_hide_dlc") then
			color_ranges = table.remove(color_ranges, 1)
		end
	end
	return text_id:gsub("^%s*(.-)%s*$", "%1"), color_ranges
end
]]