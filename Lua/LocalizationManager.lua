
Hooks:Add("LocalizationManagerPostInit", "LocalizationManagerPostInit_CrimenetRemastered", function(loc)
	local loc_path = CrimenetRemastered._path .. "Loc/"
	for _, filename in pairs(file.GetFiles(loc_path)) do
		local str = filename:match('^(.*).json$')
		if str and Idstring(str) and Idstring(str):key() == SystemInfo:language():key() then
			loc:load_localization_file(loc_path .. filename)
		end
	end
	loc:load_localization_file(loc_path .. "english.json", false)
end)

--[[
local text_original = LocalizationManager.text
function LocalizationManager:text(string_id, ...)
	if (CrimenetRemastered:GetOption("brkr_hide_dlc")
			and (string_id == "menu_ultimate_edition_short"
			or string_id == "menu_dlc_buy_ue"
			or string_id == "menu_l_global_value_ue"
			or string_id == "cn_menu_community"
			or string_id == "menu_l_global_value_pd2_clan"))
		or (CrimenetRemastered:GetOption("brkr_hide_new")
			and string_id == "menu_new") then
		return ""
	else
		return text_original(self, string_id, ...)
	end
end
]]
