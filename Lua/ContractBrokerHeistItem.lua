
local function make_fine_text(text)
	local x, y, w, h = text:text_rect()
	text:set_size(w, h)
	text:set_position(math.round(text:x()), math.round(text:y()))
end

function ContractBrokerHeistItem:init(parent_panel, job_data, idx)

	self._parent = parent_panel
	self._job_data = job_data
	local job_tweak = tweak_data.narrative:job_data(job_data.job_id)
	local contact = job_tweak.contact
	local contact_tweak = tweak_data.narrative.contacts[contact]

	local hide_image = CrimenetRemastered:GetOption("brkr_hide_image")
	local hide_contact = CrimenetRemastered:GetOption("brkr_hide_contact")
	local hide_dlc = CrimenetRemastered:GetOption("brkr_hide_dlc")
	local hide_new = CrimenetRemastered:GetOption("brkr_hide_new")

	local line_height = CrimenetRemastered:GetOption("brkr_line_h")
	local line_padding = CrimenetRemastered:GetOption("brkr_line_pad")

	local max_font_size = 22
	local max_font_size2 = 48
	local text_padding = math.max(line_padding, 3)
	local content_height = line_height - line_padding
	local two_lines = false
	local font_size = math.min(content_height * 0.8, max_font_size)
	if content_height >= max_font_size * 2 + line_padding then
		two_lines = true
		font_size = math.min(content_height * 0.8 / 2, max_font_size2)
	end
	local font = font_size > max_font_size and tweak_data.menu.pd2_large_font or tweak_data.menu.pd2_small_font

	self._panel = parent_panel:panel({
		halign = "grow",
		layer = 10,
		valign = "top",
		x = 0,
		y = line_height * (idx - 1),
		h = line_height
	})

	self._background = self._panel:rect({
		blend_mode = "add",
		alpha = 0.4,
		halign = "grow",
		layer = -1,
		valign = "grow",
		y = line_padding,
		h = content_height,
		color = job_data.enabled and tweak_data.screen_colors.button_stage_3 or tweak_data.screen_colors.important_1
	})
	self._background:set_visible(false)

	self._image_panel = self._panel:panel({
		halign = "left",
		layer = 1,
		valign = "center",
		x = 0,
		y = line_padding,
		w = not hide_image and content_height * 1.7777777777777777 or 0,
		h = content_height,
		visible = not hide_image,
	})
	local has_image = false
	if job_tweak.contract_visuals and job_tweak.contract_visuals.preview_image then
		local data = job_tweak.contract_visuals.preview_image
		local path, rect = nil
		if data.id then
			path = "guis/dlcs/" .. (data.folder or "bro") .. "/textures/pd2/crimenet/" .. data.id
			rect = data.rect
		elseif data.icon then
			path, rect = tweak_data.hud_icons:get_icon_data(data.icon)
		end
		if path and DB:has(Idstring("texture"), path) then
			self._image_panel:bitmap({
				valign = "scale",
				layer = 2,
				blend_mode = "add",
				halign = "scale",
				texture = path,
				texture_rect = rect,
				w = self._image_panel:w(),
				h = self._image_panel:h(),
				color = Color.white
			})
			self._image = self._image_panel:rect({
				alpha = 1,
				layer = 1,
				color = Color.black
			})
			has_image = true
		end
	end
	if not has_image then
		local color = Color.red
		local error_message = "Missing Preview Image"
		self._image_panel:rect({
			alpha = 0.4,
			layer = 1,
			color = color
		})
		self._image_panel:text({
			vertical = "center",
			wrap = true,
			align = "center",
			word_wrap = true,
			layer = 2,
			text = error_message,
			font = font,
			font_size = font_size
		})
		BoxGuiObject:new(self._image_panel:panel({layer = 100}), {sides = {
			1,
			1,
			1,
			1
		}})
	end

	local contact_name = self._panel:text({
		alpha = two_lines and 0.8 or 0.7,
		layer = 1,
		vertical = "center",
		align = "left",
		halign = "left",
		valign = "top",
		text = not hide_contact and managers.localization:to_upper_text(contact_tweak.name_id) or "",
		font = font,
		font_size = font_size * (two_lines and 0.9 or 0.7),
		color = tweak_data.screen_colors.text,
		visible = not hide_contact
	})
	make_fine_text(contact_name)
	contact_name:set_left((not hide_image and self._image_panel:right() or 0) + text_padding)
	if two_lines and not hide_contact then
		contact_name:set_bottom(self._image_panel:center_y())
	else
		contact_name:set_center_y(self._image_panel:center_y())
	end

	local job_name = self._panel:text({
		layer = 1,
		vertical = "center",
		align = "left",
		halign = "left",
		valign = "top",
		text = managers.localization:to_upper_text(job_tweak.name_id),
		font = font,
		font_size = font_size,
		color = job_data.enabled and tweak_data.screen_colors.text or tweak_data.screen_colors.important_1
	})
	make_fine_text(job_name)
	if two_lines and not hide_contact then
		job_name:set_left(contact_name:left())
		job_name:set_top(contact_name:bottom())
	else
		job_name:set_left((not hide_contact and contact_name:right() or (not hide_image and self._image_panel:right() or 0)) + text_padding)
		job_name:set_center_y(contact_name:center_y())
	end

	if not hide_dlc then
		local dlc_name, dlc_color = self:get_dlc_name_and_color(job_tweak)
		local dlc_name = self._panel:text({
			alpha = 1,
			vertical = "top",
			layer = 1,
			align = "left",
			halign = "left",
			valign = "top",
			text = dlc_name,
			font = font,
			font_size = font_size * 0.9,
			color = dlc_color
		})
		make_fine_text(dlc_name)
		if two_lines and not hide_contact then
			dlc_name:set_left(contact_name:right() + text_padding)
			dlc_name:set_bottom(job_name:top())
		else
			dlc_name:set_left(job_name:right() + text_padding)
			dlc_name:set_center_y(job_name:center_y())
		end
	end

	if job_data.is_new and not hide_new then
		local new_name = self._panel:text({
			alpha = 1,
			vertical = "top",
			layer = 1,
			align = "left",
			halign = "left",
			valign = "top",
			text = managers.localization:to_upper_text("menu_new"),
			font = font,
			font_size = font_size * 0.9,
			color = Color(255, 105, 254, 59) / 255
		})
		make_fine_text(new_name)
		if two_lines and not hide_contact then
			new_name:set_left(((not hide_dlc and dlc_name:text() ~= "") and dlc_name or contact_name):right() + text_padding)
			new_name:set_bottom(job_name:top())
		else
			new_name:set_left(((not hide_dlc and dlc_name:text() ~= "") and dlc_name or job_name):right() + text_padding)
			new_name:set_center_y(job_name:center_y())
		end
	end

	local icons_panel = self._panel:panel({
		valign = "top",
		halign = "right",
		h = line_height,
		w = self._panel:w()
	})
	icons_panel:set_right(self._panel:right() - text_padding)

	local last_icon = nil

	local icon_size = font_size * 1.1
	self._favourite = icons_panel:bitmap({
		texture = "guis/dlcs/bro/textures/pd2/favourite",
		vertical = "top",
		align = "right",
		halign = "right",
		alpha = 0.8,
		valign = "top",
		color = Color.white,
		w = icon_size,
		h = icon_size
	})
	self._favourite:set_right(icons_panel:w())
	self._favourite:set_center_y(job_name:center_y())
	last_icon = self._favourite

	local day_text = icons_panel:text({
		layer = 1,
		vertical = "center",
		align = "right",
		halign = "right",
		valign = "center",
		text = self:get_heist_day_text(),
		font = font,
		font_size = font_size * 0.9,
		color = tweak_data.screen_colors.text
	})
	make_fine_text(day_text)
	day_text:set_right(last_icon:left() - text_padding)
	day_text:set_center_y(job_name:center_y())
	last_icon = day_text

	local length_icon = icons_panel:text({
		layer = 1,
		vertical = "center",
		align = "right",
		halign = "right",
		valign = "center",
		text = self:get_heist_day_icon(),
		font = font,
		font_size = font_size * 0.7,
		color = tweak_data.screen_colors.text
	})
	make_fine_text(length_icon)
	length_icon:set_right(last_icon:left() - text_padding)
	length_icon:set_center_y(job_name:center_y())
	last_icon = length_icon

	if self:is_stealthable() then
		local stealth = icons_panel:text({
			layer = 1,
			vertical = "center",
			align = "right",
			halign = "right",
			valign = "center",
			text = managers.localization:get_default_macro("BTN_GHOST"),
			font = font,
			font_size = font_size,
			color = tweak_data.screen_colors.text
		})
		make_fine_text(stealth)
		stealth:set_right(last_icon:left() - text_padding)
		stealth:set_center_y(job_name:center_y())
		last_icon = stealth
	end

	local last_played = self._panel:text({
		alpha = 0.7,
		vertical = "center",
		layer = 1,
		align = "right",
		halign = "right",
		valign = "center",
		text = self:get_last_played_text(),
		font = font,
		font_size = font_size * (two_lines and 0.8 or 0.7),
		color = tweak_data.screen_colors.text
	})
	make_fine_text(last_played)
	if two_lines then
		last_played:set_right(self._panel:right() - text_padding)
		last_played:set_bottom(job_name:top())
	else
		last_played:set_right(last_icon:left() - text_padding * 2)
		last_played:set_center_y(job_name:center_y())
	end

	self:refresh()
end