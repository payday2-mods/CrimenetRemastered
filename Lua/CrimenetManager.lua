
local _create_locations_original = CrimeNetGui._create_locations
local _get_job_location_original = CrimeNetGui._get_job_location
local _create_job_gui_original = CrimeNetGui._create_job_gui
local update_server_job_original = CrimeNetGui.update_server_job

function CrimeNetGui:_create_locations(...)
	_create_locations_original(self, ...)
	local newDots = {}
	local xx, yy = 8, 10
	for i = 1, xx do
		for j = 1, yy do
			local newX = 150 + 1600 * i / xx
			local newY = 150 + 750 * (i % 2 == 0 and j or j - 0.5) / yy
			table.insert(newDots, {math.round(newX), math.round(newY)})
		end
	end
	self._locations[1][1].dots = newDots
end

function CrimeNetGui:_create_job_gui(data, type, fixed_x, fixed_y, fixed_location, ...)
	local crnt_size = CrimenetRemastered:GetOption("crnt_size")

	local size = tweak_data.menu.pd2_small_font_size
	tweak_data.menu.pd2_small_font_size = size * crnt_size
	local result = _create_job_gui_original(self, data, type, fixed_x, fixed_y, fixed_location, ...)
	tweak_data.menu.pd2_small_font_size = size

	if result.side_panel then
		local job_plan_icon = result.side_panel:child("job_plan_icon")
		local host_name = result.side_panel:child("host_name")
		if job_plan_icon and host_name then
			job_plan_icon:set_size(job_plan_icon:w() * crnt_size, job_plan_icon:h() * crnt_size)
			host_name:set_position(job_plan_icon:right() + 2, 0)
		end
		local job_name = result.side_panel:child("job_name")
		if CrimenetRemastered:GetOption("crnt_colorize") and job_name and not data.mutators and not data.is_crime_spree and type ~= "crime_spree" then
			job_name:set_color(CrimenetRemastered.diff_colors[(data.difficulty_id or 0) - 1] or Color.white)
		end
	end
	if result.heat_glow and CrimenetRemastered:GetOption("crnt_glow") then
		result.heat_glow:set_alpha(result.heat_glow:alpha() * 0.5)
	end
	return result
end

function CrimeNetGui:update_server_job(data, i, ...)
	update_server_job_original(self, data, i, ...)
	local job_index = data.id or i
	local job = self._jobs[job_index]
	if job.side_panel and CrimenetRemastered:GetOption("crnt_colorize") and not data.mutators and not data.is_crime_spree then
		job.side_panel:child("job_name"):set_color(CrimenetRemastered.diff_colors[(data.difficulty_id or 0) - 1] or Color.white)
	end
end

function CrimeNetGui:_get_job_location(data, ...)
	_get_job_location_original(self, data, ...)
	local diff = (data and data.difficulty_id or 2) - 2
	local diffX = 236 + ( 1700 / 7 ) * diff
	local locations = self:_get_contact_locations()
	local sorted = {}
	for k,dot in pairs(locations[1].dots) do
		if not dot[3] then
			table.insert(sorted,dot)
		end
	end
	if #sorted > 0 then
		local abs = math.abs
		table.sort(sorted,function(a,b)
			return abs(diffX-a[1]) < abs(diffX-b[1])
		end)
		local dot = sorted[1]
		local x,y = dot[1],dot[2]
		local map = self._map_panel:child("map")
		local tw = math.max(map:texture_width(), 1)
		local th = math.max(map:texture_height(), 1)
		x = math.round(x / tw * self._map_size_w)
		y = math.round(y / th * self._map_size_h)

		return x,y,dot
	end
end
