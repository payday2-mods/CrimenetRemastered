
local init_original = GuiTweakData.init
function GuiTweakData:init()
	init_original(self)
	if not CrimenetRemastered:GetOption("crnt_borders") then
		self.crime_net.regions = {}
	end
end
