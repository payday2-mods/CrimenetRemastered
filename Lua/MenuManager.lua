
Hooks:Add("MenuManagerInitialize", "MenuManagerInitialize_CrimenetRemastered", function(menu_manager)
	MenuCallbackHandler.callback_crimenet_remastered_setting = function(self, item)
		local name = item:parameters().name
		if item._type == "toggle" then
			CrimenetRemastered._data[name] = item:value() =="on"
		else
			CrimenetRemastered._data[name] = item:value()
		end
		CrimenetRemastered:Save()
	end
	CrimenetRemastered:Load()
	MenuHelper:LoadFromJsonFile(CrimenetRemastered._path .. "Menu/menu.json", CrimenetRemastered, CrimenetRemastered._data)
end)

Hooks:Add("MenuManagerBuildCustomMenus", "MenuManagerBuildCustomMenus_CrimenetRemastered", function(menu_manager, nodes)
	if nodes.main and CrimenetRemastered:GetOption("brkr_show") then
		MenuHelper:AddMenuItem(nodes.main, "contract_broker", "menu_cn_premium_buy", "menu_cn_premium_buy_desc", "crimenet", "after")
	end
end)

-- fix 'allow mutated lobbies' being greyed out when 'friends only' is ticked (a vanilla bug)
local update_node_original = MenuCrimeNetFiltersInitiator.update_node
function MenuCrimeNetFiltersInitiator:update_node(node, ...)
	update_node_original(self, node, ...)
	node:item("toggle_mutated_lobby"):set_enabled(true)
end
